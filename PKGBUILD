# Maintainer: David Runge <dvzrv@archlinux.org>
# Maintainer: Sven-Hendrik Haase <svenstaro@archlinux.org>
# Contributor: Lauri Niskanen <ape@ape3000.com>
# Contributor: Sebastian.Salich@gmx.de
# Contributor: Doc Angelo

pkgname=mumble
pkgver=1.5.735
pkgrel=4
pkgdesc="An Open Source, low-latency, high quality voice chat software"
arch=(x86_64)
url="https://www.mumble.info/"
license=(BSD-3-Clause)

depends=(
  abseil-cpp
  libpipewire
  libsndfile
  libx11
  libxi
  openssl
  opus
  poco
  protobuf
  qt5-base
  qt5-svg
  speexdsp
)

makedepends=(
  boost
  cmake
  microsoft-gsl
  nlohmann-json
  python
  qt5-tools
)

source=(
  "https://github.com/mumble-voip/mumble/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz"{,.sig}
  always-break-line-when-sending-message.patch
)
sha256sums=('db8990079f556a877218d471bcf2c24eb5e4520b652f3c20793d0aadedaae6ae'
            'SKIP'
            '2ed220bc641cf1bdf10e503f840b672bac73acfcad9755f9ec75e42181418324')

# See https://github.com/mumble-voip/mumble-gpg-signatures
validpgpkeys=(
  '9B9ADC09AD09F76B10F87CBFCDB285AE2332CF8D'  # Mumble Automatic Build Infrastructure 2024 <mumble-auto-build-2024@mumble.info>
)

prepare() {
  cd "$pkgname-$pkgver"

#  patch -Np1 -i "$srcdir"/always-send-messages-in-legacy-format.patch
  patch -Np1 -i "$srcdir"/always-break-line-when-sending-message.patch
  sed -e 's|CMAKE_CXX_STANDARD 14|CMAKE_CXX_STANDARD 17|' -i CMakeLists.txt
}

build() {
  cmake -B build \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D CMAKE_BUILD_TYPE=None \
    -D BUILD_NUMBER="${pkgver##*.}" \
    -D client=ON \
    -D pipewire=ON \
    -D xinput2=ON \
    -D alsa=OFF \
    -D asio=OFF \
    -D benchmarks=OFF \
    -D BUILD_OVERLAY_XCOMPILE=OFF \
    -D bundled-gsl=OFF \
    -D bundled-json=OFF \
    -D bundled-renamenoise=OFF \
    -D bundled-speex=OFF \
    -D coreaudio=OFF \
    -D crash-report=OFF \
    -D debug-dependency-search=OFF \
    -D display-install-paths=OFF \
    -D elevation=OFF \
    -D g15=OFF \
    -D g15-emulator=OFF \
    -D gkey=OFF \
    -D ice=OFF \
    -D jackaudio=OFF \
    -D manual-plugin=OFF \
    -D online-tests=OFF \
    -D oss=OFF \
    -D overlay=OFF \
    -D overlay-xcompile=OFF \
    -D plugin-callback-debug=OFF \
    -D plugin-debug=OFF \
    -D plugins=OFF \
    -D portaudio=OFF \
    -D pulseaudio=OFF \
    -D qtspeech=OFF \
    -D renamenoise=OFF \
    -D retracted-plugins=OFF \
    -D server=OFF \
    -D speechd=OFF \
    -D static=OFF \
    -D symbols=OFF \
    -D tests=OFF \
    -D tracy=OFF \
    -D update=OFF \
    -D warnings-as-errors=OFF \
    -D wasapi=OFF \
    -D xboxinput=OFF \
    -D zeroconf=OFF \
    -S "$pkgname-$pkgver"

  cmake --build build --verbose
}

package() {
  DESTDIR="$pkgdir" cmake --install build
  install -vDm 644 "$pkgname-$pkgver/LICENSE" -t "$pkgdir/usr/share/licenses/$pkgname/"
}
