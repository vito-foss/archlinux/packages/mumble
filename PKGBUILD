# Maintainer: Sven-Hendrik Haase <svenstaro@gmail.com>
# Contributor: David Runge <dvzrv@archlinux.org>
# Contributor: Lauri Niskanen <ape@ape3000.com>
# Contributor: Sebastian.Salich@gmx.de
# Contributor: Doc Angelo

pkgname=mumble
pkgver=1.3.4
pkgrel=1
pkgdesc="An Open Source, low-latency, high quality voice chat software"
arch=('x86_64')
url="https://www.mumble.info/"
license=('BSD')
depends=(
  gcc-libs
  glibc
  lsb-release
  openssl-1.1
  qt5-base
  hicolor-icon-theme
  protobuf
  libpulse
  libsndfile
  libx11
  libxi
  opus
  qt5-svg
  speex
  xdg-utils
)
makedepends=(
  boost
  protobuf
  python
  qt5-tools
)

source=("https://github.com/mumble-voip/mumble/releases/download/${pkgver}/mumble-${pkgver}.tar.gz"{,.sig}
        '0001-Added-DBus-calls-to-activate-and-deactivate-push-to-talk.patch')
sha512sums=('e2dde5aad30d1a931e7acd8d7db0b8c785d3a268d0b8cf5626fa03953a51bcadb719c6682c440d02524e83beee13d73d8f5bb3fdf98ab1c82d3ecf824cc25f68'
            'SKIP'
            'ca751eb688e9de88064e2e11f155b4c69a55c9464fcd839ad8ce834af7e444a5683be62700fe7e6e7a60a5de37c0dc48575bb81d633607974e0f41d05b6a51be')
# See https://github.com/mumble-voip/mumble-gpg-signatures
validpgpkeys=('6A24D225AAE02DD6352C3D269F179B6BF48ADF25') # Mumble Automatic Build Infrastructure 2021 <mumble-auto-build-2021@mumble.info>

prepare() {
  cd "$pkgname-$pkgver"

  patch -Np1 -i "$srcdir"/0001-Added-DBus-calls-to-activate-and-deactivate-push-to-talk.patch
}

build() {
  cd "$pkgname-$pkgver"

  export PKG_CONFIG_PATH=/usr/lib/openssl-1.1/pkgconfig

  qmake-qt5 main.pro \
    CONFIG+='no-bundled-opus no-bundled-speex \
             no-g15 no-xevie \
             no-server no-embed-qt-translations \
             no-update no-crash-report \
             no-bonjour \
             no-jackaudio no-oss no-alsa no-speechd \
             no-overlay \
             packaged \
             bundled-celt' \
    DEFINES+='PLUGIN_PATH=/usr/lib/mumble'
  make release
}

package() {
  cd "$pkgname-$pkgver"
  # mumble has no install target: https://github.com/mumble-voip/mumble/issues/1029
  # binaries and scripts
  install -vDm 755 release/mumble -t "$pkgdir/usr/bin"
  # (vendored) libs
  install -vdm 755 "$pkgdir/usr/lib/mumble/"
  local _lib
  for _lib in release/*.so*; do
    if [ -L "$_lib" ]; then
      cp -vP "$_lib" "$pkgdir/usr/lib/mumble/"
    else
      install -vDm 755 "$_lib" -t "$pkgdir/usr/lib/mumble/"
    fi
  done
  install -vDm 755 release/plugins/liblink.so -t "$pkgdir/usr/lib/mumble/"
  # XDG desktop integration
  install -vDm 644 scripts/mumble.desktop -t "$pkgdir/usr/share/applications"
  # AppStream metadata
  install -vDm 644 scripts/mumble.appdata.xml -t "$pkgdir/usr/share/metainfo"
  # man page
  install -vDm 644 "man/${pkgname}"*.1 -t "$pkgdir/usr/share/man/man1/"
  # XDG desktop icons
  install -vDm 644 icons/mumble.svg -t "$pkgdir/usr/share/icons/hicolor/scalable/apps/"
  # license
  install -vDm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
